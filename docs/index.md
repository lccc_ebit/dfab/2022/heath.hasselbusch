# Home Page

## "*Nothing in the world is worth having or worth doing unless it means effort, pain, and difficulty*" -Theodore Roosevelt


![](./images/index/Studious.jpg)


Welcome to my Home Page.  Feel free to browse through my virtual museum to view my accomplishments.

Currently, I am working on a Mechanized Beyblade Launcher, and my progress will be posted in the assignments section.

Throughout these 16 weeks of the Spring 2023 semester, I'll be posting my progress on this project.




 > This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>