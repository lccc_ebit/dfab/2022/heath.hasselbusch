# Week 7: Making Progress

***April 1st-April 7th***

## Beyblade Launcher Mach II

With the evidence I collected, I got back to work designing another bottom base, this time allocating for the following:

- Increased inner diamater inside motor hold
- Larger holes for cords to fit through back end
- Enhanced ventilation surrounding motor area
- Rounded edges to improve ergonomics

This time all I needed to print is the bottom base; the top part should fit on the same, if not better because I changed all the fillet radiuses to be the same.  The print took 13 hours this time, and once again I took it off the Ender bed on lab Thursday the 6th.  This time however, I got a much different result:

<iframe width="560" height="315" src="https://www.youtube.com/embed/sTZYGZ1Y8U8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

It works!!! I almost couldn't believe it, but I had to.  All my hard work and experimenting payed off, and I can finally say that I have a working prototype.

The base is noticably more stable. Now that there's some airflow inside and the motor isn't squeezed in there against the PLA plastic, I can launch all my Beyblades at 3x the speed I was before; this was spinning way faster than when I had it connected to a 9V battery.  

Granted I still haven't ran a test with the top part screwed on, but it's because the alligator clips I used for the connections don't exactly hold well together, and wiggle out of place after a few launches.  Still, as long as I could use it this way, all that's left to do now is design...yes...the PCB board that'll go inside.

Here's another video showing me launching all my Bey's at once:

<iframe width="560" height="315" src="https://www.youtube.com/embed/u1-tWvzarn8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

This works great! These things spin like crazy and hold pretty well while getting up to speed.  I've negated the time it took to put the ripcord through the launcher, and I can launch them all with ease, not to mention 3x faster than they're supposed to XD

## PCB (Prelude)

It took me a good 2 weeks to come up with a board, and during this week I focused on researching more boards and using other peoples schematics to come up with my own

Starting off the list of changes to my components of my pcb board; I don't think I'll be using a 9V battery anymore.  I would be changing them batteries every time I used this thing, so I switched tactics and decided I was going to use a wall outlet now to power my circuit.  It'll provide a more powerful, consistent source of energy without me having to buy/use a bulky voltage supply to power this thing.  

I also won't be putting led lights on to indicate the speed of the motor, but this would be a design feature I'd include in my 2nd launcher that I'll make in the future ;) It would've been too much to work around and my pcb board got crowded quick.  You'll see later why I had to dumb down my board because of time constraints.

I'll say this right now, it was a lot easier coming back to the pcb board aspect after I'd figured out the rest of my project.  Truth be told, I don't think I could've started out with designing the board because if I did that before creating a prototype, it'd be like working in reverse.  How would I know if the chip would work if I wasn't able to replicate what I'm trying to achieve.  Now that everything else was out of the way, I was thinking much more clearly about my board.

I'll end this portion here, but next week is going to have all my PCB board stuff


> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
