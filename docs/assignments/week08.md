# Week 6: Starting to get a *Handle* of things

***March 24th-March 31st***

Finally, on March 27th, I took my print off the F370, and officially had my first prototype of the motorized Beyblade Launcher:

## Images of Beyblade Launcher Prototype Print.  

![](../images/week08/IMG_0763.jpg)
![](../images/week08/IMG_0764.jpg)
![](../images/week08/IMG_0765.jpg)

Right off the bat you'll notice a few things wrong with my print.  For one the edges are too sharp, and it just doesn't look ergonomic for anyone to hold.  The biggest drawback however, rendered the print useless; the motor hold was way too small to house the 12v motor.

![](../images/week08/IMG_0756.jpg)
![](../images/week08/IMG_0755.jpg)

Not only that, the area inside where the pcb board would sit was designed way too small.  I went back and made some changes to my design, including:

- Enlarged interior shell to provide more room for the 9V battery & PCB board
- Ventilation for PCB/9V battery (located in back of the model)
- Screw insert holes and ventilation holes on where the motor sits
- Reverting back to top/bottom half design for simplicity
- Heavy focus on making sure the holding area for the DC motor is suitable
- Ergonomic features to ensure comfort during use
- Improved dome feature

I wanted to really make this something you could just hold in your hand and *feel* the power of the Beyblade in your hands, and that the user would be confident & safe behind this mechanism.  

So I went back into Solidworks, designing a new handle that should work even without the top casing on it.  I should be able to get some footage of it all working together.

## Beyblade Launcher Mach I
On March 30th, I recieved my print(s) from the Ender Pro's. The bed size was hardly big enough to fit one of the pieces at an angle, let alone being able to print 2 large jobs at the same time probably would've taken a whole day to complete.

Instead I ran the top and bottom parts on separate machines so both can only focus on printing one thing, which also in turn reduces the time to print greatly.  I set both of them to print on the 29th around 8pm and by the time I got to lab the next morning they were complete:

![](../images/week08/IMG_0778.jpg)

*Top half is White, Bottom half is Yellow*

I really like how this came out.  I had designed the feature for the top part to slide over that skinny extrusion on the bottom part, and it fits on really well.  I almost don't need any hardware for it to stay on.  After taking it on and off so much it started loosening up.

![](../images/week08/IMG_0779.jpg)

The motor home area came out really good too; the motor slid right in, but it was a tight enough fit that it wouldn't wobble around inside.  All the holes lined up, and so I went ahead with the first round of tests.  But in classic capstone fashion, another problem was created.  See for yourself;

<iframe title="vimeo-player" src="https://player.vimeo.com/video/818126761?h=ae266d6765" width="640" height="360" frameborder="0"    allowfullscreen></iframe>

## Troubleshooting

I expected this design to work, so I was surprised when I did my first test and it made that noise.  Everything was balanced, that is except the holding apparatus for the Beyblades.  In a slow motion capture of just the apparatus spinning on the shaft while held to the body, there's a slight imbalance that can only be noticable when slowed down.  Maybe when the Beyblade was hooked on it caused that end where all the action is happening to be unbalanced; violently shaking with the load of the bey.

I tried clamping it down in a vice, playing with different voltage settings, and even drilled holes through the side 
Do I need to stabalize the shaft more? Does the motor sit in an awkward position that I couldn't see through the interior? Is there enough air going through the motor? I was feeling *stressed*, but I knew I had to deal with this problem the same way I've handled all my problems in this class; by attacking them head on and staying confident...

Nothing was making sense to me; how was it that I could use this mechanism outside of the case, but not when it's tightly secured in it's own area.  My hypothesis was that perhaps, since there isn't any air movement within the base where the motor sits, it's almost like the motor is suffocating itself inside the printed base.  

When taken out of the base, there's no restriction to it's airflow, and if you look inside the yellow base you'll notice a lack of air holes *around* the motor.  I accounted for ventilation where the motor would be seated, but didn't think that restricting air flow could cause the motor to lose efficency.

With this in mind, I decided to start experimenting with different ways of holding the motor/launching the Beyblade.

## Experimenting 

In my first experiment, I placed the motor vertically into the base, this time flipping the yellow base upside down so its hanging down, not inside the body.  When I did this, it launched the Beyblade as it would've if I held the motor in my hand:

<iframe title="vimeo-player" src="https://player.vimeo.com/video/818136480?h=d7d75553b7" width="640" height="360" frameborder="0"    allowfullscreen></iframe>

Right away you can see and hear the difference in the launches.  Compare this footage to the previous close-up footage showing how unbalanced it was.  This time it's quieter, gets up to the speed I need (with a 9v battery I should metion) and launches it like it would've launched if I was just holding the motor (like before).  I believe the reason for it being so loud when the motor is oriented the way it's supposed to be, is because of how cramped the motor was inside that hole.

This meant that it wasn't the design of the base entirely, just where the motor was sitting; proving that all I might need are some airways.

My other theory; was that maybe that hole I made for the motor to fit in is still really claustrophobic.  That, along with the lack of ventilation around the motor might be playing hand in hand in restricting the efficeny of the motor.  So in my second experiment; my physics instructor and I decided to test whether or not pressure around a motor effects it's speed.

To test this, we grabbed a motor he had in the backroom and connected it to power; recording the fastly rotating shaft's frequency with a sensor to detect any sort of resistance. The highere the frequency, the higher the resistance.  My hypothesis is that when we put the motor under stress, the frequency will be higher than the other tests because the components inside are being compressed.

Based on the motor's orientation, and surrounding pressure, here are our findings:

![](../images/week08/IMG_0898.jpg)


In the first 2 trials, we ran the motor upside down and right side up (right side up meaning shaft pointing down).  The distinction between the 2 frequencies weren't all that surprising, however a blurb in the right side up trial peaked our interest; maybe when the motor faces down it experiences some resistance from something inside the motor sitting on top of something else perhaps?

We ran another test, but this time didn't get that hiccup in the data.  We suspect this hiccup in the first trial could've been some external force that the sensor picked up.

But what we found interesting was the data collected from when pressure was applied to the outside of the motor.

In this trial, we ran another test holding the motor in it's downward position to determine if that unexpected blurb was truly a hiccup, or part of the motor:

![](../images/week08/IMG_0899.jpg)

Turns out, it was just a hiccup.  Something else must've been detected on the sensor, however what WAS true was that applying pressure on the motor DID make a difference in the performance of the motor.

Granted the motor we used wasn't one I planned on using in my project, and we didn't attach the beyblade apparatus to the end, but the logic should still hold true that encasing a motor in a tight area without anywhere for the air to escape/move through can affect it's performance.

Next week, we'll have a new bottom base, with some updated features.  Hopefully I get a start on the PCB stuff because I'm only about a month and a few weeks away from graduation.  I'm glad I was able to prove my hypothesis correct through scientific experimentation, and if all goes according to plan, this new base should be much more stable, quiet, and potentially capable of achieving higher rpm's.

> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
