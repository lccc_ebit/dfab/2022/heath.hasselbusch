# Week 10: Vinyl Designs, CNC Arena Cutout, and More PCB...

***April 23rd-April 30th***

##  Intro

With the end of the year nearing, I really need to kick it into gear with my pcb part.  I keep getting hung up on how I'll control the 12V thats supposed to power the motor, when the potentiometer is recieving only 3.3V from the voltage regulator.  


![](../images/week12/IMG_1009.jpg)
![](../images/week12/IMG_1012.jpg)
![](../images/week12/IMG_1013.jpg)
![](../images/week12/IMG_1014.jpg)
![](../images/week12/IMG_1015.jpg)

To my dismay, many of these boards I've cut out, soldered, and attempted programming have all failed.  This design included.  Most of them have been problems that occured while soldering and cutting off extra material which can cause shorts.  I failed multiple times; I cut traces with the exacto knife on accident, pulled up pads by heating them too much with the solder gun, and on a few occasions the mdx-50 mill was the cause of failure.  Even if I made a circuit without messing anything up, 2 of them shorted on me when I plugged them in.  The one that never shorted while plugging in; shorted after programming.  That is the circuit you see in the diagram below.
![](../images/week12/drawn_board.png)
![](../images/week12/LDA-LM3480.png)
The reason for this board's failure is attributed to the voltage regulator.  The pins in the schematic don't match the pins in the datasheet for the LDA-LM3480 voltage regulator.  Looking at the schematic, you'll see the pin numbers are switched:
![](../images/week12/wrongtraces.png)

This mistake caused the ATTINY 1614 to overheat, but I was running out of time to meet the other requirements of my project.

##  Vinyl & CNC

I used the roland gs-24 vinyl cutter to cut out my designs.  I'll print some more out on May 8th before the project is due.  All I'm doing is Beyblade text on the top part of the launcher with flames around the side of the bottom part.

The other requirement I stated I'd meet was to cut out something on the Forest Scientific CNC Router.  My plan is to cut a 2ft by 2ft piece that I can screw my arena onto, and have a slot in the front for my launcher to fit nice and snug.

![](../images/week12/IMG_0990.jpg)

![](../images/week12/IMG_1018.jpg)

The job took not even 5 min.  You can actually see the Beyblade vinyl text on the top of the launcher.

Now all that's left is to figure out this darn pcb board.  That and print out another handle that'll look sleek.

> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
