# DFAB 221 Fall Semester Virtual Archive

This is an archive of what my previous website looked like throughout every week circa Fall 2022.  For those unaware, the website has been re-structured for documentation of the Beyblade launcher project.  

## Week 2: Project management

This weeks instruction has us installing GitLab, creating a repository, and getting started and comfortable with the basics of GitLab/MK Docs

For a quick guide on the basic commands I used to create my website, visit [markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/).  These are  basic commands that I used to create my website.

### Coding the Website
The first step to create a repository on GitLab, was to of course install GitBash. Depending on what computer you're using (Windows/Mac/Linux)
Then create a username for the account in MK Docs: 
![](../images/week02/git_username.jpg)

You will also need to configure an email address. 

![](../images/week02/git_email.jpg)

If a past SSH KEY isn't already configured (or you are unsure if there is one or not) typing```cat~/.ssh/id_rsa.pub``` will check for an SSH KEY.  To generate an SSH KEY, type ```ssh-keygen -t rsa -C **"$your_email**```into MK Docs.

My example: 

![](../images/week02/gitlab1.png)

Then copy the key you just created: ```clip<~/.ssh/id_rsa.pub```
From here, just add the key you just copied into your GIT site.  Here's a video for further explanation:

<iframe width="560" height="315" src="https://www.youtube.com/embed/54mxyLo3Mqk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Copying & Uploading Your Online Repo

You want to make sure you have a designated file.  It is highly recommended that this file is created & designated somewhere obvious, aka the desktop.  This would prevent you having to search through multiple file locations to access this repository.  In short: designating this repo to your desktop would be a wise decision for many efficiency reasons.

Then, you'll clone your repo by typing into MK Docs:
![](../images/week02/git_clone.jpg)

You should then see your file appear in the desktop of your computer:
![Something similar to this](../images/week02/desktop.jpg)

Depending on what syntax processor you're using (I'm using Brackets), you'll be making your edits to the website through that program, and when you want to actually push those changes up to the live website, you'll go into MK Docs to complete the process. 

Again, the one program (Brackets) is used for making the edits to your website, like adding photos, text, videos, etc. The other (MK Docs) is the system we're using for pushing those changes we've made up to the database we've linked our files to on our newly created local computer servers.

Now, we get into the nitty gritty of what this process will look like.

### Editing Your Website

By opening your designated repo file that should be on your desktop, right clicking the file titled "docs" that should open up a bunch more folders (but don't open the file just right click on it) it should look something like this:

![](../images/week02/git_bash_here.png)

Click on the tab that says Git Bash Here.  This will open your live MK Docs page where my acronym is going to come in handy.

### To insert an Image:  

```![alt text](image.jpg)```  You can use pictures from your phone, or take screenshots on your computer using the snip tool.  Uploading pictures from an external device takes a few more steps.  For this you'll have to upload a picture from your phone to something like Google Drive.  From there you'll have to insert that image into a word document and resize it to fit the webpage without taking up too much space.  Images speak a thousand words, so take plenty of pictures to save time typing out what you're trying to convey.

Make sure when you save the pictures that they're being saved to the correct week:

![](../images/week02/docs_main.jpg)

![](../images/week02/docs_images.jpg)

It's important to keep your files organized, otherwise you'll risk losing track of your files/images. 

Using links to other websites and videos is also crucial to making a good website.  They provide easy-access information that would be unnecessary to include in your webpage.   

Link: ```[title](https://www.example.com)```

Embedded Youtube Video: <iframe width="560" height="315" src="https://www.youtube.com/embed/IAu4KcermpE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

There are many ways to include a video in your repo, but Youtube is the easiest way to have a video in your website.  You can even embed the video into your website so the user doesn't have to leave the page to watch the video.  You can copy and paste my text above, seeing the only change you'll have to make is the video code (```IAu4KcermpE```)

Youtube gives each video its own special code.  The code for this video.  What's the video code you may ask? Simple, its ```IAu4KcermpE```.

![](../images/week02/youtube.jpg)

Other than that, coding a website isn't that difficult.  It takes a while to get used to typing so much code to input an image, video, or link.  I found it easier to copy the code from previous modules and just edit the code so it's the correct week with the corresponding image.

### SAC-M P! (Status.Add.Commit.Push)

So you've made your first edit, great!  The first step is to check the **Status** of your files.  To do this, type ```git status``` to check what's been changed:

![](../images/week02/git_status(1).jpg)

Notice the red text? Don't panic, this just means the system recognizes you've made changes, however these changes haven't yet been added/given an explanation as to what the change was (git add/git commit -m).  This completes the first of 2 **Status** commands. 

The clever acronym I've come up with for how to get into the routine of checking the **Status** of what you've edited, how to then **Add** the files, correctly directing the changed file.  **Commit**, or in other words explain what the change was, the -M part of this acronym goes along with **Commit** I'll explain it later.  And then finally **Push** up those changes to GIT Lab.  The more this step is repeated, the more natural it'll come every time you go to save.  Trust me it takes some getting used to, having a separate program to further-more save your changes, but it's like this for a cool reason, you'll see.

The next step is to **Add** those changes to where you want them.  Simply complete this step by typing ```git add ``` and typing exactly what's in red.  An easy way to do this is just pressing (tab) and the program will automatically fill in the text:

![](../images/week02/git_add.jpg)

After ```git add```, check ```git status``` again to check if you did it correctly.  The red text should now pop up green like this.  If not, go back and make sure you typed everything in correctly.

Here is where we will **Commit**, or in other words explain what the changes you made consisted of.  To do this, type ```git commit -m "whatever change you made in your files"```  You'll type whatever message you want to send within those quotation marks, that'll summarize what you did.

![](../images/week02/git_commit.jpg)

Make sure not to forget ```-m``` after typing ```git commit```, if you don't, you'll get this page and this is not where you want to be:

![](../images/week02/git-m.png)

If you stumble across this like I did, go ahead and type whatever you intended on writing, followed by pressing the ESC key and typing :wq.  This will essentially complete the ```git commit``` process, but it's a much more drawn out way of going about it.

The final step: type ```git push``` to send everything you've done up to GIT Lab.  

-----------------------------------------------------------

## WEEK 5: Electronics Production (Fall 2022)
This week's goals are to:

Design circuit boards using mods CE to generate traces, outlines, and toolpaths,
test our knowledge on sending said toolpaths to the MDX-50 mill, and learning how to run the Model A MDX-50 mill.

### Intro

In this week's instruction, the DFAB 221 Fall Semester class had to learn the process of creating a trace/outline in mods CE.  Mods CE is a free, online website for creating various designs & creations.  This isn't a program you download, it's literally a website.  - [Click here for mods CE](https://modsproject.org/)

To get to where we need to be in order to generate our files/settings, we need to:

1. Right Click/2 Finger Click to bring up this menu, Click "Programs"

![](../images/week05/programs.jpg)

2. Open Program

![](../images/week05/open_program.jpg)

3. Scroll down until you see **Roland**, and select the only option in the **MDX mill** section titled **PCB**.  
![](../images/week05/RolandMDXPCB.jpg)
This is because we are using an MDX-50 mill to cut out PCB's (Printed Circuit Boards)

4. If you followed the steps correctly, you should be here.  Notice you're in the right spot if the big text displays **Roland MDX PCB** like it does at the top of this example 
![](../images/week05/rolandmodsmain.jpg)

### Traces/Outlines

Instead of designing the traces and outlines, we're going to the -[Fab Academy Website](https://fabacademy.org/2022/), click schedule to pull up the Fab Academy's 2022 Schedule.  From here, scroll down to where it says **Mar 16**, and click on the link for **embedded programming**.  This is where I found my traces for my PCB board:
![](../images/week05/hello.D11C.serial.5V.1.1.traces.png)
Download the corresponding outline png file 
![yes I'm aware the file says interior](../images/week05/hello.D11C.serial.5V.1.1.interior.png)

These are gonna be the files we plug into mods CE to continue the process.

### Setting up the file

Now we should be back on the mods CE website with our downloaded PCB trace and outline, and if you didn't close out of mods CE you should still see the Roland MDX PCB page from before.  Otherwise, repeat the steps listed in the intro to get up to pace.

Click **select png file**

![](../images/week05/pngfileselect.jpg)

Find & select your trace file, as this will be what first gets cut on the mill. Depending on what file you saved (svg/png), you'll have to decipher which "path" you'll have to take.
In the name of keeping things simple, it's recommended that the traces are completed first, since this will be the first thing you cut on the MDX-50 mill. The outline will cut *out* whatever traces that were just milled.  The only settings we're worrying about here will be the cut/max depth, origin offset, and the model.  Any setting that's doesn't get addressed here, should be LEFT ALONEEEEEE.

After selecting the trace file, your screen should look display the file like this:

![](../images/week05/traces1.jpg)

Notice the blue string(s) running all over the screen, and how it's almost designed like a tree with branches?  You'll follow those blue lines to where you need to add your next bit of information, so navigate to where it says **set PCB defaults** in pink boxed lettering, and change the cut/max depth from ```0.004``` to ```0.005```under the mill traces section.

![](../images/week05/milltraces.jpg)

Afterwards, click mill traces(1/64), since we're using a 1/64 tool bit to cut the traces.  You're on the right track if you see this appear in the **distance transform** section:

![](../images/week05/distance.jpg)

Then go to the **mill raster 2d** section.  Click **Calculate** and an automatic browser should pop up displaying the generated path.

![](../images/week05/calculate.jpg)

Next, follow over to the **Roland MDX/ iModela** section.  The settings that we need to change for this section are as followed: 
- model: MDX-20>MDX-40
- Origin: x:10>0  y:10>0
The reason we change these settings is because whilst we can't select the MDX-50 as our model, the MDX-40 is the next best thing.  As for the origin; we change this setting because if you don't, this is how your pcb board turn out:

![](../images/week05/wrongpcb.jpg)

Although this was a failed attempt, my next attempt failed because there was a problem with the machining bed for the MODEL A MDX-50.  Chris and I observed the bed after the second pcb was cut and noticed wobbling when light pressure was put on the bed.  Here are both failed cuts:

![](../images/week05/both_fails.jpg)

The first attempt (shown right) initially had the wrong settings applied prior to cutting.  The second attempt (shown left) had correct settings, yet still contained not-so-subtle imperfections, being the unfinished cut in the bottom left-hand corner. Due to the unstable condition of the machine bed, it would flex the copper material that was stuck to the wooden bed if pressure was applied.  This would explain why the center of the board looks more finished, and the bottom left corner looks somewhat raw, not to mention shiny.

To fix this, the DFAB instructors replaced the old bed with a new one.  This new black, new material felt much more dense than the wooden base whose place it took:

![](../images/week05/newbase.jpg)

I was able to finally cut out a proper pcb board, and here is what the end result looked like:

![](../images/week05/goodpcb.jpg)

Notice off the bat the difference between this pcb board and the failures, is that bottom left corner.  The black is much cleaner and defined than before, meaning it cut evenly.

### Soldering Components

After cleaning off the excess copper, it's time to get some components soldered on.
Here's the board that was just cut out.  The green text represents the components we'll need to configure this board.

![](../images/week05/finalPCB.jpg)

1. (2) 0Ω Resistors (R1 & R2), and (1) 4.99kΩ Resistor (R3)
2. (1) 4 pin SWD (J1)
3. (1) D11C (IC1)
4. (1) 1 uF Capacitor (C1)
5. (1) 3.3V Microchip (IC2)
6. (1) 6 pin Serial (J3)
*final pcb shown above*

I rekindled my soldering skills and got to work:  I started with the resistors, mainly because resistors are the cheapest and easiest to solder of these other components.  

![](../images/week05/resistors_soder.jpg)

I went ahead and soldered a few more components, like the (C1) capacitor and the 6 pin Serial (J3):

![](../images/week05/soder2.jpg) 
![](../images/week05/soder3.jpg)

I also soldered the 3.3V IC2 and if you look close enough, you'll notice a sliver of solder material crept down the path.  If this happens to you it's fine, as long as no solder bridges over other components.  Bridging can be described as protruding material that connects to other pins.  I accidentally bridged some material across the pins of my 6 pin Serial (J3), and had to use copper wire to remove the excess material.  Fortunately everything was ok.

Here's what my board looked like after completely soldering on all the components:

![](../images/week05/soder5.jpg)

Don't forget to use Flux when soldering, it helps the solidifying process.  Also, DON'T INHALE THE FUMES!!!  It's toxic, and trust me you don't want to know what it feels like to inhale the soldering fumes.

![](../images/week05/finalPCB.jpg)

*Hero Shot*

---------------------------------------------------------
## WEEK 7: PCB Design (Fall 2022)

### Intro

This week, we're designing our own PCB board, fitted with 2 push buttons and 2 LED's. Our learning outcomes are as follows:
-Design a PCB Board/Schematic on Eagle Software
-Grab traces/outlines and prep file in mods CE
-Cut traces/outlines on the MDX-50
-Solder components 

### Downloading Eagle

To complete this lab, download the newest Eagle Cad software from Autodesk.  Make sure it's the free version.

[Download Eagle for Free Here](https://www.autodesk.com/products/eagle/free-download)

![](../images/week07/download_eagle.jpg)

If you have a Window's software computer, select the option for Windows, etc.  Click "Next" for all the prompts until you reach the home screen.  I recommend putting a check mark next to where it asks "Create Desktop Shortcut" otherwise you'll be searching through files to open EagleCad.

![](../images/week07/eagle_home.jpg)

Once on the home screen, follow these prompts as ordered: File > New > Project > Give project a name (name it "test")  > Right click "test" > New > Schematic.  Now you should have a new schematic to start designing.

You'll also need a board to layout your schematic design.  If this doesn't make sense just hold on, I'll explain in a second, just click this button to create your board:

![](../images/week07/sch_brd.jpg)

This concludes the setup portion of this index, here's a video summarizing the text above in case if there was any confusion:

<iframe width="560" height="315" src="https://www.youtube.com/embed/hYVQ5IdzsRg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Designing

The concept of this lab is to design our own traces and board/schematic, as well as program it.  Week 5 demonstrated the process of taking trace/outline files to modsCE, where we configured the appropriate settings to cut out a functional pcb board.  This week, we're adding the step of **creating** a schematic yourself.

Before we dive any further, here's a list of part's we'll need for this circuit:
![](../images/week07/parts.jpg)

To add parts, select the icon that has a green plus, and what appears to be an outlet plug of some sort.  From there you should be greeted by hundreds of files; scroll down until you find the file titled **fab**.  From here, use the list of parts shown above to add the correct parts to the schematic.  To draw connections between each component, click on the icon with a green, 90° angled line from the left-hand side menu.  

A few important notes for designing:

- Don't forget to place net junctions (green dots) where parts of the circuit traces intersect/meet.  This is done with the net junctions command, found underneath the green line tool previously shown.  For example; the middle (2) pin for the 3-pin receiver will act as a running ground line in this schematic; place net junctions as needed to identify merging traces

- Be mindful of how the layout of the component(s) pins are set up.  The layout of the attiny1614's pins are organized differently in the schematic, but the board displays the pins in the way we'd perceive these microchips in person.  More detail on this later...

Here's how my design turned out:
![](../images/week07/schematic.jpg)
![](../images/week07/full_board.jpg)

Notice the difference between how the schematic (top) is layed out, compared to the board version(bottom) of itself. 
The board pins better represent the physical pins locations and connections.  Like stated earlier; the schematic layout is going to be messy and confusing to get used to, but the board view lays out the traces.

The green lines we drew in the schematic window aren't actually the final traces lines, they just represent the connections.  In the board window, we'll actually be designating the path of these traces with red lines.  These paths are more realistic; being able to twist and turn to create the most efficient path to each connection.  

Before exporting these files, turn off all the layers except for the outline and trace layers.  The file needs to only have the traces and outlines.  If we were to keep the wording & other marks shown visible, and exported the files those features would get cut on the mill, wording and everything.  Seeing we only need traces and outlines (which we aren't cutting out this chip like before, but it's good practice to have both files) hide all the layers except for the 2:

![](../images/week07/board.jpg)

*Scroll down all the way and shift click to select all the files, select hide layers, go back up and make top and dimension visible*
Once that's done, you can export files by clicking; File>Export>Image

Set resolution to 1000 DPI, make sure the box for **monochrome** is checked, and save the images in a place you'll find them easily.  Press "Ok" when finished.

### modsCE Setup

Once you have your 2 exported png files, import them to the modsCE software, and we're following the same process as we did in week 5; creating the trace/outline files for the MDX-50. Follow these instructions:

### Traces

- Select the traces png file 

![](../images/week07/traces1.jpg)

- Keep settings the same in the *set PCB defaults* section, just click *mill traces (1/64)*

- Calculate traces 

![](../images/week07/traces2.jpg)
Double check for how the software auto-generated the traces  Any discrepancies must be resolved, meaning a redesign of your board layout.  I had this happen to me so it could happen to you!  

![](../images/week07/traces3.jpg)

- Keep the 2nd and 3rd buttons turned on for the last section of the modsCE web, leave the first one off.

### Outlines

- Select the outlines png file 

![](../images/week07/outline1.jpg)

- Keep settings the same in the *set PCB defaults* section, just click *mill outline (1/32)*

- Calculate outline 

![](../images/week07/outline2.jpg)

- As before, the 2nd and 3rd buttons are turned on, leave the 1st button off.

For now just cut out the traces.  We probably won't cut out the outline file but its important for you to know how to do both files.

### Problems

Unfortunately, something was wrong with the Roland MDX-50, and my circuit-board was cut out incorrectly.  The problem I was having was that the head wasn't deep enough to sufficiently cut out the traces.  I had this happen on my first board I cut: The machine polished the traces, instead of cutting them down to the proper height.  To avoid time constraints, my classmate cut out a duplicate of his design for me to use.  

Here's that board in EagleCAD software

![](../images/week07/craig_board.jpg)

Realistic view of traces and components:

![](../images/week07/before_sodering.jpg)

Final Product after soldering:

![](../images/week07/after_sodering.jpg)
----------------------------------------------------------
## WEEK 9: Embedded Programming (Fall 2022)
This week, we're programming our boards to do whatever we want it to do.  The main objective of this exercise is for you to play around in Arduino with this board.  Try to experiment with different ways to make the LED's behave differently, as well as the buttons.  

### Introduction

To complete this portion of instruction, download the apporpriate Adurino software by clicking the link below:

[click here to visit download page](https://www.arduino.cc/en/software)

I downloaded the 2.0.2 version for windows, but I ended up having to download Arduino  1.8.19 because the programmer package we have to download doesn't show up in version 2.0.2.  For that reason, I'd recommend downloading version 1.8.19.

After installing Arduino 1.8.19, go to Tools>Boards Manager>Search for MegaTinyCore by Spence Konde:

![](../images/week09/Tools_BoardsManager.jpg)

![](../images/week09/MegaTinyCore.jpg)

Go back to the Tools tab and where it says "Programmer:", select the option for "SerialUDPI - SLOW: 57600 baud".  After this you should be ready to code.

### Programming

Arduino's reference guide is rather helpful for understanding the basic and advanced commands: 

[click here to visit Arduino's reference page](https://www.arduino.cc/reference/en/)

I played around with a few commands to get used to the software, these are some basic commands you'll more than likely use in programming your first board.

![](../images/week09/Arduino_1.jpg)

The set of commands listen in this capture include: #define, pinMode, INPUT/OUTPUT, PULLUP. Use Arduino's website for reference and look up these terms. Pro tip>holding Ctrl+F to filter search the page.

![](../images/week09/reference.jpg)

This first section of code will basically be the setup section of the program, under the ```void setup``` section.  If you're finding it hard understanding ```#define```, all it does is rename the numerical value of the components.  In other words; ```Green_led``` was originally named 3.  This is because the Green Led is getting power from the 3rd pin on our board.  Same goes for the Red Led, it's connected to pin 2 on the microchip.  

As for both the buttons; same ordeal as before.  Button 1 is connected to pin 9, and Button 2 is connected to pin 8.  This is how I learned to understand it.

### **NOTICE**
Do not forget to put brackets & parenthesis where necessary/needed
Don't forget to put semi-colons at the end of every line of code (except ```if``` and ```else```)

Here's the second part of my code:

![](../images/week09/Arduino_2.jpg)

This section is labled ```void loop```.  This program sets a loop occurrence for your codes to consecutively change and respond to whatever you designate it to do.  This first part of my code utilizes Button_1 to initiate a pattern I made.  

When Button_1 is pressed ```if(digitalRead(Button_1)==0)```, The Red LED will turn off ```digitalWrite(Red_led,HIGH);``` for 5 seconds ```delay(5000);``` and the Green LED is on ```digitalWrite(Green_led,LOW);``` until it's 5 second cycle is over.  After that, the Red LED will turn on ```digitalWrite(Red_led,LOW);``` and the Green LED will turn off```digitalWrite(Green_led,HIGH);```. The terminology may be confusing at first given that "HIGH" turns off the LED, but it's because the resistor is essentially activating, blocking current from reaching the LED.  So when it goes down to "LOW" that means the resistor isn't applying any resistance, allowing current to reach the LED.

Once again pay close attention to the placement of where my brackets and parenthesis are, as it applies to you too.  

![](../images/week09/Arduino_5.jpg)

This code uses Button_2 to do a flashing program so that the Red and Green LED's alternately flash .2 seconds each, for 10 cycles(#define makes this easier for us: we can #define flash as a value of 10).  ```for(int i=0;i<flash;i++)``` is a basic loop.  Whenever i reaches 10, it'll stop. After it completes each loop, i increments by 1 (i++).  The loop ends when (i) < 10 (i becomes 10, so its no longer smaller than 10, therefore ending the command).

If this gets too confusing to wrap your head around, stackoverflow.com is a good website for quick coding answers.  [click here to visit stackoverflow.com](https://stackoverflow.com/)

The code displayed underneath after the 2 (//) was my old code that carries out the same exact function as the one I just showed you.  The first way is a much more efficient way to code, the second way is a beginners way of tackling the same obstacle.

![](../images/week09/Arduino_3.jpg)

If neither button recognize an input of any sort, in other words not being pressed or held, both light's will remain on.  This else command doesn't just complete the ```if``` command, but serves a purpose in indicating that the board is emitting power. 

![](../images/week09/Arduino_4.jpg)

This is the final result of my program:

<iframe width="560" height="315" src="https://www.youtube.com/embed/wVH2pZWBI14" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

----------------------------------------------------------

## 11. Input devices

This week, we're going to learn about the Hall Effect Sensor, and use it in designing and programing another board.

### Introduction

This module is very short.  The process is exactly like week07/week09, where we designed schematics and board's in EagleCAD, generated our traces & outlines in modsCE, soldered components accordingly, and programmed the board to detect a magnet in Arduino

But you might be asking, what exactly is a Hall Effect Sensor?  Hall Effect Sensors are used to detect a disturbance in magnetic fields.  

### EagleCAD

Here's a list of components we'll be using for our schematic:

![](../images/week11/listofparts.jpg)

Originally we were going to use at412's instead of 1614's, but there were some complications with downloading the proper components to Arduino.

Here's an image of my board design:

![](../images/week11/Halleffect_sensor_HH.jpg)

![](../images/week11/Halleffect_schem.jpg)

Make sure you keep a trace width of 16 in the board settings, this will ensure that when your traces cut out that they'll be deep enough to allow current to flow.

Repeat the steps in week05 to retrieve your trace and outline files from modsCE, as usual none of the settings are changing except the x/y offsets on the 4th table from 10-->0

![](../images/week11/modsCE_traces.jpg)

![](../images/week11/modsCE_outlines.jpg)

### Disclamer:  

Make sure you secure your material on the bed ALL THE WAY.  When I was cutting out the outlines on my board, my material popped out during cutting and it could've caused a great deal of issues not just for me and my assignment, but the whole machine.  

### Soldering

![](../images/week11/explodedview.jpg)

![](../images/week11/1st_solder.jpg)

A big mistake I made here early on in my first attempt at this (yes I had to cut out a completely separate board because of this mistake).  I flipped the attiny 1614 so that it was opposite of what it should've been.  This caused my led on the pcb board to light up as soon as we plugged it into a power source; something we don't want in this case.  

![](../images/week11/2nd_solder.jpg)

I also had another issue where the pads on the pcb board stuck onto the components if I held the solder iron on for too long:

![](../images/week11/problem_pads.jpg)

Apply some hot glue to the 3 pin connector to ensure the pins won't pull up, you can see the glue in the hero shot of the first board here:

![](../images/week11/hero_shotBLU.jpg)

Shown above is my first board.  This turned out to be a faulty board because I flipped the attiny 1614, which is why the led immediately turned on when it's connected to power.  Shown below is the 2nd board I cut out.  I didn't have to hot glue gun the 3 pin connector on this one because it made a solid connection when I first soldered it.

![](../images/week11/heroshot2.jpg)

### Arduino

The final step is to program the board.  Shown below is the code for my board:

![](../images/week11/Arduino_Code.jpg)

This simple program demonstrates how Hall Effect Sensors work with the help of a magnet.  The closer the magnet gets to the sensor, the brighter it gets.  Inversely, the led will get dimmer as the magnet moves away from the sensor.  The video below demonstrates that program:

<iframe width="560" height="315" src="https://www.youtube.com/embed/IAu4KcermpE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

-----------------------------------------------------------



> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
