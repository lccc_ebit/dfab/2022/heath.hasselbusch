# Week 5: Solidworks Design for 3D Fastener & Remote Base.

***March 9th-March 23rd***

## Spring Break (3/10-3/20)

Finally...the break I needed.  

Before recording my video launching multiple Beyblades, I noticed the fastener started experiencing some warping. This deficiency caused the Beyblade to spin more unleveled when preparing to launch, and when I'd take power off, it wouldn't shoot out unless I shook it slightly. I was running myself into the ground until this point because I was so frustrated with not understanding the project's PCB concept, and not having a set Beyblade launcher wasn't making me feel any better.  

This wouldn't fly for me, and thus started the trials of 3D fasteners;

## Trials of the 3D fasteners

Though I was lucky in my first attempt at printing to make a part that worked (for as long as I needed it to work), there was no avoiding this problem. I wasn't surprised about the part deforming, but what surprised me was how fast it deformed. A slight cause of this could've been after the part was taken off the printing bed; I applied heat to the plastic to create the press-fit and manually screwed a screw in the other hole to make the threads. I believe all that contortion could've deformed the part even before the first-ever test and only worsened the more it was used. I had plenty of time to get it right; I knew my idea would work; it just needed some more tweaking.

Another leading cause of the fastener's failure could also be because of the printer. The nozzle on the Ender's are only 2mm in dimension, and seeing that my fasteners were only a few (mm) in dimension, I was told that I was fortunate enough to even come out with a clean part that worked. So I redesigned the holes so that one side was bigger~more accurate to what the press-fit hole size would be, and the other side was just the right size so when I screwed the screw in, the hole would be large enough that the screw makes its way through more straightforward, and at the same time small enough so the threads are precise.

I started with the Markforged 3D printer since the nozzle was smaller and would provide a more accurate print. My theory was I could make the press-fit hole closer to the size of the motor shaft and make the screw insert hole small enough to make precise threads but big enough so it was easier to screw it down. My first print didn't come out well because I printed it on its side, and when I printed it out the second time, it still came out pretty messy. Pictured below is my old fastener that got warped (white) and the part created by the Markforged (black):

![](../images/week07/IMG_0706.jpg)
![](../images/week07/IMG_0708.jpg)

I definitely can't trust this machine to get the job done the way I want, so I went another route with the resin printer. This method would print the parts I need to the exact dimensions with a more robust material than the Ender or Markforged.  

Here's the part that came off the resin printer:

![](../images/week07/IMG_0707.jpg)

The resin printer takes an easy win over the Markforged in terms of printing a clean part; however, when examining the Markforged print, you'll notice that the end fits nicely in the clutch, unlike the resin printer print. 

When I used the resin printer, I used the same strategy before of printing different scaled fasteners to have a larger sample size. None of these fasteners, except for the largest scaled fastener, were suitable for use until I discovered the insert failed at...inserting. I was back to square one; printing more fasteners on the Enders was my cheapest option. At least I knew it was possible to make a working part from this printer because I had done it before.  

Luckily, since the original scale was still suitable for the clutch insert (as demonstrated in the Markforged print image), I will redesign one of the ends to match the thickest diameter in the center. This will make Ender's job easier in creating the hole feature, and I can pilot the hole at the other end for the screw insert if it doesn't come out clean.

![](../images/week07/IMG_0702.jpg)
"`This one, I actually heated the plastic on the end that's visibly deformed, and while it worked for a little bit, I ultimately need a fastener that'll press right in without having to do any scraping or heating to make it work. ```
![](../images/week07/IMG_0701.jpg)

 The two shown above in the cup don't fit the clutch insert, nor were any of the holes suitable. It's a mystery as to which scaled version worked, but I believe it was the smallest one. I also managed to print one out later on that worked with the help of some superglue, but that didn't last too long either.

I at least knew by this point I could come back to this when I needed to. I could start focusing on other parts that needed to be taken care of before April came. My goal is to start really hammering the PCB board part of this project come the end of March-beginning of April. This way, I can accomplish other tasks in the meantime, just like I've said before, only this time I started designing my handle for my controller.

## Getting a *Handle* of Things

The design of the handle has been my favorite part of the project so far. I think it's cool that I'm 3D printing a remote that controls a DC motor; it's definitely not something you get the chance to do every day, that is, have access to this technology and be able to create something like this.  

![](../images/week07/IMG_0699.jpg)
"`sliced model of left/right base` "

I originally designed this to be a top and bottom half. when I superglued the fastener to the motor shaft, I didn't think at the time about how I was now going to force something of the same diameter, also connected to the motor shaft, through the bottom of this base .This meant I had to redesign it to make it left/right halves so I could just sandwich the motor launching apparatus inside. However, the superglue wore off, and since the design I printed shown above was too small, I could go back to my old design (and that makes printing way easier)

A fortunate little mishap that was all for nothing; now, if you want to read the long version, be my guest:

## Designing (& Redesigning) the 3D Base

![](../images/week07/IMG_0755.jpg)

On March 17th, I took this part off the F370. The job. Even though this wouldn't be my final iteration of my project, this sideways design provided some more insight on how I should design my second remote, as this one had a lot of problems with it
 
A glaring problem right from the start is how cramped the inside of the remote is. For one, there isn't enough room to put the motor in, or even the PCB board for that matter. Even the back holes where my speed control would connect were too small.  

![](../images/week07/IMG_0756.jpg)

Going back to the problem I had when supergluing the shaft and launching apparatus together since the glue didn't stay and the print wasn't suitable for use, it meant I could add features I previously didn't include. Some of you may be asking why I didn't just go back and redesign my old part again. Would it make sense to re-redesign that part file to its original format? No, not to me.  

Not to mention, the Solidworks lab was closed on March 23rd, so I wasn't able to do anything...I was stuck. That was until I found a way to get a license for Solidworks 2021-2022 on my laptop so I could make changes whenever I wanted to. The only problem with this is that I can't open files I used in the Solidworks lab because I was using a previous version, henceforth the need to re-create a whole base from scratch with updated features.

Some of these new features would include:
- a vent-style back to provide more ventilation for the PCB board
- redesigned motor cockpit with new ventilation and screw insert features
- smoother contours along the sides so nobody is potentially injuring themselves using this contraption
- a redesigned ramp system for the motor connection

There could be more additions to the design because I now have the luxury of doing Solidworks at home, a huge time-saving convenience for me.

## PCB

During our spring break, I met with a buddy of mine. I went to the Medina County Career Center within the Engineering Technology & Design trade, which was really good with electrical components and stuff of the sort. We sat down March 12th to discuss the matter.

Here's the datasheet of the ATTINY 1614 microchip
![](../images/week06/1614pinout.jpg)
And here's the datasheet for the A4950 motor driver
![](../images/week07/A4950.jpg)

And here, you see a brief description of what my board should look like. As I've been saying, nothing has been confirmed, but this is the best I got so far:

![](../images/week07/IMG_0753.jpg)

Some of the pinouts make sense to me, but right now, I'm still figuring it all out. I hope to meet with this friend of mine more to get more work done on it.

I do know I want to designate this circuit to run in parallel, meaning 9V of electricity is distributed throughout the entire board. I'll run aground and power train from the headers connected to the 9V battery. The IRFZ44N voltage regulator will be placed before power can reach the 1614 chip and regulate the voltage to 5V to the 1614 chip while allowing 9V to run straight to the a4950 motor driver.

![](../images/week04/IMG_0594.jpg)

So with this potentiometer in my Carrera Go! remote, I'll add a third wire for the analog input of the 1614, with an output running from the 1614 to the motor driver that'll analyze the pulse and control the motor. The ATTINY 1614 will be respondant to this remote, or at least I believe how this should work out...

I'm not sure what resistors or diodes, or capacitors to be using, but I have a general idea of what I might need based on what other people used.


> This is the end of the page

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
