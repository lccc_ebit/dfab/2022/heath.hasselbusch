# May 7th-May 13th

On May 9th I presented my project to my instructor Scott Zitek.  While most of my project was good, I didn't accomplish motor control from my chip.  Even though my board works as shown below: It hasn't started working the motor yet.  This is obviously a big part of my project, but so big that it could cause my entire project to be all for nothing if this doesn't work...

<iframe width="560" height="315" src="https://www.youtube.com/embed/hPhUi041g1c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/et7CbTJEGAA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/NGX_lzeQbCU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/o40wkxr32Sc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/QZYe0NTKCQo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I'm not sure if this is going to be taken for credit or not, but I'm trying to get with one of the instructors before the semester ends to get that figured out.

## Final B.O.M.

1. Jameco #23247 12V Brushed DC Motor  ```1 provided for free courtesy of Fab Lab LCCC, two extra motors purchased 3/12/2023 and received 3/16/2023. ``` [Jameco #23247 Motor](https://www.jameco.com/z/MD5-2445-Jameco-ReliaPro-12-Volt-DC-Motor-19-850-RPM_232047.html)
2. A4950 Motor Driver ```provided, courtesy of Fab Lab LCCC``` [A4950 Digi-Key](https://www.digikey.com/en/product-highlight/a/allegro-microsystems/a4950-dmos-full-bridge-motor-driver-ic)
3. AtTINY 1614 SSN Microchip ```provided, courtesy of Fab Lab LCCC```  [ATTINY 1614 Digi-Key](https://www.digikey.com/en/products/detail/microchip-technology/ATTINY1614-SSNR/7354616)
4. Voltage Supply ```provided, courtesy of Fab Lab LCCC```
5. 3D printed products as needed ```provided, courtesy of Fab Lab LCCC```
6. Copper Board ```provided, courtesy of Fab Lab LCCC```[Copper Stock](https://www.googleadservices.com/pagead/aclk?sa=L&ai=DChcSEwizuLDY5-r-AhXufG8EHfBLA04YABAMGgJqZg&ase=2&ohost=www.google.com&cid=CAESauD20YSQSI5MmLaAd1ROZgKj7XThBiM945cO1TnvDRn9qUUTsZ8iF9e1o-sgaqIdTUFaLSCB_rJfC5YAcbLcJjn4lTNRfHcs9xZhhJFLKW14aFbeGyawUsabrDnYXikQ1sBwXW8aY0xbLI0&sig=AOD64_3Fc6oKjt6ZFBJ2rHWNkxw4Qlj9AQ&ctype=5&q=&nis=4&ved=2ahUKEwjR1anY5-r-AhWVJUQIHTFMCBUQ9aACKAB6BAgCEA4&adurl=)
7. LDA-LM4380 3.3 Volt Regulator  ```provided, courtesy of Fab Lab LCCC``` [LDA Regulator Digi-Key](https://www.digikey.com/en/products/detail/texas-instruments/LM3480IM3X-3-3/3697028?utm_adgroup=Texas%20Instruments&utm_source=google&utm_medium=cpc&utm_campaign=Dynamic%20Search_EN_Focus%20Suppliers&utm_term=&utm_content=Texas%20Instruments&gclid=CjwKCAjwge2iBhBBEiwAfXDBR97QOpRHifYJewcfVuZjUz7fZZqjhJSks2XkLdf1OHSsmX41adUJmhoCyaAQAvD_BwE)
8. (x3) .1 uf Capacitors
9. (x1) 10 uf Capacitor
10. 2.1 mm Comm Jack ```provided, courtesy of Fab Lab LCCC``` 
11. (x3) 2pk of Super Glue ```Dollar General, dontated to DFAB LAB afterwards```
12. (x2) 6 Pin Headers ```provided, courtesy of Fab Lab LCCC```
13. (x1) 3 Pin Connector  ```provided, courtesy of Fab Lab LCCC```
13. (x1) 1k Resistor  ```provided, courtesy of Fab Lab LCCC```
14. (x1) Green LED  ```provided, courtesy of Fab Lab LCCC```



> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>