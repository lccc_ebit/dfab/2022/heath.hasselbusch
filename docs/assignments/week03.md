# Week 1 The Beginning of the End.  

## Graduation Project DFAB 231 Spring Semester 

## **Intro** 

***February 2nd-10th, 2023***

I spent the whole semester working on a final project to receive the credits for this class and be closer to earning my Associates in Applied Sciences & Digital Fabrication. This last project had to utilize multiple processes I learned throughout my years of learning at LCCC. While I played around with a few ideas, I finally came up with the idea of making a battery-powered, dc motor-controlled Beyblade launcher.  

Actually, I got the Beyblade launcher idea from one of LCCC Fab Lab LA's, Natalie. We were discussing what I could do, and I eventually switched gears and just started talking about something else with them. Our conversation eventually led to the topic of childhood toys and past-times. Beyblades got brought up in conversation, and she offered the idea of a motorized Beyblade launcher. I instantly went with the idea, seeing that I was struggling to even come up with an idea.

While the idea of re-vamping the Beyblade launcher isn't a new concept, I would be the first (from my extensive knowledge and research searching Google & youtube videos) to create a launcher that utilized multiple different manufacturing processes to do a quality project.

***Disclaimer(s)*** 

It took me until February 10th to get started on the Beyblade launcher concept. This is primarily due to me and my lab partner working on our group project until February 2nd. 

With this in mind, I'd like to note that each assignment tab will be about a week's worth of progress. Some weeks may be longer than others, and some weeks might end up getting grouped together, but for the time being, while I'm roughly documenting, I'll be basing my on progress pictures/videos I took (trust me I took a ton)

------------------------------------------------------------------------------------------------

When looking for the earliest evidence of researching concepts/personal drawings in my photos, the earliest image I have is from a video I watched where someone tested out a mechanical gear ratio Beyblade launcher:

![](../images/week03/Gearratio1.png)

```Date video & photo was assessed:`February 10th, 2023```

I started by taking apart an old R.C. car to understand how a motor would connect to a P.C.B. board and how a motor would drive a gear train, or in my case, control the speed and launch of a Beyblade. For reference, I took apart this Hurricane Rc Car:

![](../images/week03/hurricane.jpg)

From here, I started formulating a plan; what I'd tackle first, extensive research, etc.
Before I get too far ahead, here's my supposed B.O.M.

## Bill of Materials & More...

While this is a partial Bill of Materials, I knew that in the end, I would need more parts than I initially thought, but it could also be less. With that being said, here is my current estimated-to-be Bill of Materials (as of 3/16/2023) *=unsure regarding qty or $$

1. Jameco #23247 12V Brushed DC Motor  ```1 provided for free courtesy of Fab Lab LCCC, two extra motors purchased 3/12/2023 and received 3/16/2023`. ``
2. A4950 Motor Driver ```provided, courtesy of Fab Lab LCCC```
3. AtTINY 1614 SSN Microchip ```provided, courtesy of Fab Lab LCCC```
4. Carrera, Go! slot car remote (1/43 Scale) ```personal item```
5. 9V Battery ``` USD 4.45 per unit, current tally-3```
6. 3D printed products as needed ```provided by Fab Lab LCCC```
7. Copper Board ```provided, courtesy of Fab Lab LCCC```
8. IRFZ44N Voltage Regulator
9. 10k Resistor* x1
10. 1k Resistor* x3*
11. 4x4 Header*
12. Capacitors & Diodes are known to be needed, but unsure of what type
13. Alligator Clips ```provided, courtesy of Fab Lab LCCC```
14. Super Glue ```Dollar General```

***This list will see changes periodically***

There's only evidence proving the existence of *mechanical* Beyblade launchers, not *mechanized* ones like the one I'll be making. One guy did make one with an extremely long handle, like really long. I previously thought I would've been the first, but someone did make one with an extremely long handle. However, the problem I noticed with it was that the handle was roughly 3 feet long. Beyblades would also fly out of the arena upon contact with one another. This individual created this ten years ago, and while they did a great job on their design, I want to control how fast the Beyblades spin.

What separates my design from the rest of the competition is the electrical and motorized components of the launcher. The difference is that a mechanical Beyblade launcher wouldn't be using electircity, or by textbook definition: a characteristic of someone (or here a Beyblade launcher) that manually does labour for a living(in this case, launches a Beyblade at high R.P.M.s). You can watch that video here: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Yc0YP6YjuOA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

From here, I created the most impressive invention I've ever dreamt of creating.
From here I started formulating a plan; what I'd tackle f
> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
