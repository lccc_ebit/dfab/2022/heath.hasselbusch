# Week 3: Gantt Chart, 3D Fastener and Beyblade Holding/Launching Apparatus in the Works

***February 23rd-March 2nd***

## Prelude:

This week I created a Gantt chart to organize myself and my thoughts. The idea behind a Gantt chart is to manage your ideas, chart a course, and stay on schedule. I went and did this, listing the weeks I'd need to focus on PCB board stuff, 3D printing, etc...but needless to say, it was challenging to work within the constraints you put yourself to in the beginning of the semester. I fell out of the Gantt chart schedule during spring break because I chose to focus on other aspects of my project. There would be no educational benefit in showing that document. However, keep in mind that this Gantt chart wasn't supposed to be followed perfectly. It still got me thinking about everything that needed to be done and helped me determine whether or not my goals were achievable.

## Goals

The first thing I needed to figure out was "how am I going to make/acquire something that'll connect to the motor's shaft, spin/shoot a Beyblade out, and be efficient enough to be capable of all that, plus not spontaneously combust during use.  

My first thought, stemming from disassembling my RC car to understand how a DC motor is powered/distributing power throughout the system, was to have a gear ratio attached to the motor shaft. The idea would be press-fitted onto the motor shaft with a gear at the end, powering another gear that was part of a holding apparatus for the Beyblade (which I still needed to get to this step), and subsequently launching the Beyblade.  

I certainly didn't want to get ahead of myself designing the PCB first... so I started de-constructing an old Beyblade launcher to understand how that worked. It was essential to figure this out first because if I didn't, how else would I know that everything would've appropriately worked, even if this was the only step I would've messed up on? After some tinkering, I learned that the screw insert hole of the original Beyblade launcher's inside gear component (shown below) was the perfect press-fit for the shaft of the motor I was working with:

![](../images/week05/22-2/IMG_0747.jpg)

## *Important Information*
Screw outer thread dimension: 2.94-2.96 millimeters
Screw inner thread dimension: 2.55 millimeters
Motor shaft dimension: 2.28-2.30 mm

That gear is what provides the force needed to launch a Beyblade with the launchers they make; the ripcord is fed through the launcher and lines up with this gear's teeth. That tall part of the geared part clicks into that clutch that sits inside the blue hooked teeth part of the launcher that actually holds the Beyblade, and there's a screw that holds that all together. Then there's a little spring-clip mechanism that, when there's no more ripcord to be pulled through the launcher, a little plastic bit that's spring loaded kicks out and stops the gear from spinning. That clutch quickly hits these walls placed inside the blue part that aids in quickly stopping the rotation of the Beyblade, effectively launching it fast enough for battle:

![](../images/week05/22-2/IMG_0704.jpg)

As you may have noticed in this image, there is no white gear part anymore. That's because I 3D printed that first white piece on an Ender 3D printer, copying the feature of the gear part that clicked into the clutch and adding another hole on the other end for a screw insert:

![](../images/week05/22-2/IMG_0703.jpg)

The one pertaining issue, though was that the capabilities of the Ender might prevent it from creating a good part to use, so lab aid Duncan printed 2 extra, scaled-up versions of my original file. It was good that we did that because it ended up being the largest (3rd) print that was suitable for testing (shown above). After applying some heat to the print to create the insert feature, we finally had something that hopefully would launch my Beyblade:

![](../images/week05/22-2/IMG_0705.jpg)

Prior to this, I'd also played around with the idea of machining a piece of metal w/ screw inserts on the side to keep everything together, tapping the motor's shaft to create a female piece that screwed onto the shaft. My issue with this was that the extra weight would really slow down the motor and not give the motor enough leeway to stop fast enough and launch the Beyblade out. This just seemed too complicated and risky overall, as opposed to just 3D printing sacrificial plastic parts that would be expendable and easily replaced after too much contortion. Below is a sketch of what the imagined fastener would've looked like had I committed to that idea:

![](../images/week05/22-2/IMG_0600.jpg)

This would only be the first of many fasteners to be printed out...not to mention the only one that really worked...but it's suspected there are other factors playing into this...but for now, at least, I have a way that my Beyblade is going to rotate as fast as I want it to so that it'll be faster than what I could personally spin it at.

## PCB...Ughhhh

Ahhh, my nemesis...PCB and electronics.

This has been my hardest task of this whole project and part of the reason why I'm not following my Gantt chart schedule anymore. XD. I struggled so hard with figuring out how the connections and everything will work. This was when I started thinking about it, but I would come to learn that this was going to take a lot more time than I originally planned for. Nonetheless, I watched a ton of Dc motor control videos, which to my dismay, mostly used 555 timers to control everything. I'm using an ATTINY 1614 microcontroller and an A4950 motor driver, and even till the end of March, I still haven't fully figured out everything, but I'm working on it...

With that in mind, I don't want to spend too much time on the electronics, seeing that I'll be making itsy bitsy baby steps every week on this part...as much as I hate it has to be this way...some things in life are uncontrollable, and you just have to deal with it and live with it.

![](../images/week05/22-2/IMG_0623.PNG)
![](../images/week05/22-2/IMG_0624.PNG)
![](../images/week05/22-2/IMG_0625.PNG)

My PCB board should use most of the components in this guy's example, but I'm still unsure about the final details.

## Moment of Truth Pt.1

So now the moment of truth...to see if my launcher will shoot out the Beyblade automatically since I'm using the same components in the original Beyblade launcher, just modified to a DC motor and not a ripcord, or if I'll need to compute some sort of code to rapidly slow down the rotation of my motor to give the Beyblade a proper launch. See the results for yourself:

<iframe width="315" height="315" src="https://www.youtube.com/embed/OeUDrC5XcPw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Honestly, I couldn't believe that it worked that way. I thought for sure I was going to have to incorporate some sort of electrical braking so that it could launch outright, but luckily I understand physics enough to know that when the voltage is dropped from the battery, the smooth ramped sides of the blue gripper cause the Beyblade to slip out once there's a change in an objects circular acceleration, in this case, the motor shaft stopping fast enough to launch the Beyblade out.

I write this down as a success.

> This is the end of the page

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>