# Week 8: PCB...For Real This Time

***Arpil 8th-April 14th***

Now...it's time: Making the PCB board.

## Interlude

This without a doubt was the hardest part of the whole project.  I've already had experience with 3D printing, and I've been doing this Gitlab documentation so much that I've started to get comfortable doing and figuring out new ways of coding a website.  This however would be a big step out of my comfort zone.

I've learned about electric components and microcontrollers in the last 2 years I've been at LCCC, but needless to say I wish I played with electricity more before this.  I created PCB boards in my fall semester of 2022 (all my documentation from that semester can be found in week 2 titled "Virtual Archive").

I've expressed my concern over the faitful day that is today; getting around to creating the board.  Mostly this is because I was so stressd out thinking about everything else I had to do in my project, that I didn't stop to just focus on one aspect first, before moving on to the next.

Well now we're here, and truthfully, I'm much more composed and confident in my ability to create this.  

More often than not in life we find ourselves putting on our horse blinders to focus on accomplishing a task.  That's the case for me anyways.  When a million thoughts and ideas start colliding in your brain, it can seem impossible to accomplish that task because of how many steps it's gonna take to get there.  I had to teach myself to disect problems into it's smaller parts, in order to better understand how everything comes together.

I'm definately one who looks at a problem and tries to solve it in whole.  By conditioning myself to tackle one thing at a time, I found it easier to generate concepts without getting mixed up thinking about what's next.

## Getting Started

Once again, I took to the internet to find examples of people using a4950 motor drivers, attiny 1614's, and/or controlling a 12v DC motor.  Instead of looking for someone who used these exact components all together, I observed how people were pinning each individual component, and attributing those connections with my schematic.

Using that 9v battery was convenient, but not quite strong enough to achieve the speeds I'm looking for.  This being said, the circuit I made to control this launch sequence was a simple button switch circuit, connected to the voltage supply and dc motor.  This also made me re-think the idea of using a potentiometer from an old slot car remote of mine; the B10k potentiometer was more complicated under the surface of just seeing it as a speed controller.  In turn, I'm going to now use a button switch (similar to the circuit I used to launch the Beyblades from the red base).  Now instead of being able to variable control the speed, when I press down the button it'll output between 10v-12v (depending on how powerful I want this to be) and bring the Beyblade up to speed.

I wish I could use that B10k potentiometer, however it would've created a bigger, yet unnessecary problem when all I need to do is get it up to speed.  I'd rather just press and release a button to launch a Beyblade, rather than have the ability to launch the Beyblades slower than I intended to launch them, kinda defeats the whole purpose of this concept in my eyes.

Here is my updated B.O.M. for the PCB board:
*Disclaimer, all parts were attainable through LCCC's Fab Lab

- (1x) ATTiny 1614 
- (1x) A4950 motor driver
- (1x) LDA Voltage Regulator
- (1x) 3-pin connector for programming Arduino
- (2x) 2x3 pin headers, 1 for the button switch, the other to transfer data from the motor driver to the 12v motor
- (1x) Green LED to indicate power
- (1x) 1k Ω resistor
- (1x) 1uf capacitor
- (1x) 2.1mm conn-jack


## Designing the PCB

Here's my first iteration of my PCB board:
![](../images/week10/schematic.png)

![](../images/week10/oldboard.png)

This design was too large to be cut on the copper stock we had, not only that, but it was faced horizontal.  Why is this a problem you may ask?  It's because the sacraficial material on the Roland MDX-50 is facing vertical.  At first I couldn't figure out how to rotate everything; I thought I could only rotate individual components.  I was fortunately mistaken; to rotate a group of components in eagle all you do is group select everything, select the move icon, right click to rotate.

After rotating the board, I went to cut it out.  Then...another problem reared it's ugly head.  When I saw ugly, I really mean it.

![](../images/week10/IMG_0864.jpg)

Despite setting my origins at different points, it continued to account for something in the file that wasn't supposed to be there.  I worked with Chris Leon to figure this problem out, and we discovered the text for the 3pin connector was way off to the side, and so that's what was causing the issue with setting the origin.  

![](../images/week10/board.png)

After deleting the text (yes I know it's shown here but I deleted it) it was still registering the white part of the 3pin connector, but I wasn't worried about the MDX-50 cutting this feature out because even though it somehow made it's way through (despite me hiding the layer) the mill is smart enough to not cut invisible things.

![](../images/week10/generated_traces.png)
![](../images/week10/generated_outline.png)

I made some more modifications, mostly making the board more compact to fit the size of the sacraficial.  

Throughout all this time I thought the design of the board was going to be the hardest part, boy I was horribly wrong.

## Sodering

So here's the board with all the components I'll use:

![](../images/week10/IMG_0865.jpg)

Here's another view of the traces:

![](../images/week10/traces.png)

I didn't know at the time, but it took me 4 days worth of cutting out boards, attempting to soder, and repeatedly failing over and over and over again.  I nearly lost my sanity because every time I had to cut a new board, that meant I had to 0 the z-axis for both tools (1/64 bit and 1/32 bit).  Alone the process of setting up the origins and just getting the job ready to cut took 5-10 min each time.  Then the cutting time for the traces took about 15-20 min.  

So I had to do one job where I 0-ed the 1/64 bit and cut out the traces, then I had to change the tool to the 1/32, re-zero it's z-plane, and just hope the double sided tape holds on long enough so the board doesn't get damaged while cutting.

I must've cut out nearly or a few more than 10 boards between Monday and Thursday.  2 of them included the text I mentioned before so those were completely unusable, 4 of them had traces break on me when exacto-kniving the remaining copper, 2 boards had their pads ripped up because I'd apply too much heat when sodering.  I attempted to fix the traces when I broke them by taking some copper tape and sodering it over the broken trace, but I just ended up making more work for myself in the long run doing it that way.  

One board in particular really broke me down emotionally because I had it all sodered together, and when I plugged it in, I heard a sizzle and saw a puff of smoke come out of the 2.1mm power jack...I shorted the circuit.  That was about halfway through all my trials, so I failed on sodering and trimming the copper on other occasions after that.  
![](../images/week10/IMG_0867.jpg)

I spent all day Thursday cutting out boards too, hoping I'd come home with at least something.  Starting to run out of copper (and time) I said to hell with making it look pretty & taking off that extra copper, and proceeded to my 10th (and final) cut.  

Both of the bits on the Roland MDX-50 broke on me during use, especially when the 1/32 one broke as it was cutting a board...that one got messed up.  I had to change the tools and hope that I can just hurry up and get this board already.

Alas...the pcb board came off the mill, I didn't exacto-knife the edges, and went right to sodering.  

Without further ado...I give you:

![](../images/week10/IMG_0901.jpg)
![](../images/week10/IMG_0902.jpg)
Yes...the pcb board.

What a monumental moment.  This was undoubtedly my biggest challenge of this whole project, but I stuck with it and didn't give up until I had what I wanted.

So for now the only thing left to do is code the program in Arduino and make some stickers to put on for decoration.

It's all coming together.....


> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
