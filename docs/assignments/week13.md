# Week 11: May the 4th Be With (Me)

***May 1st-May 6th***

## Very Little to Hope for....

With only a week left to work on these projects, I had to go into hyperdrive to finish these last few tasks. 

I made a new launcher design with the following changes:

- Sunken in part for PCB board to fit in
- Lined up sides of walls to prevent under-extrusion again & created enough space to place pcb board in
- Added potentiometer section on top part

I also made a new PCB board with the following changes:
- 3 More capacitors (including 1 10uf cap to the motor driver)
- Traces Vref to 3.3V. LSS to motor drier thermal pad & ground
- Utilized more pins for voltage and ground.

![](../images/week13/NewLayoutDrawing.png)
![](../images/week13/5.2.23Generatedoutline.png)
![](../images/week13/5.2.23Generatedtraces.png)


![](../images/week13/IMG_1050.jpg)
![](../images/week13/IMG_1051.jpg)

This time, I was able to get the PCB board blinking!

<iframe width="560" height="315" src="https://www.youtube.com/embed/AQBo2JeD1bw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


However, something is still wrong with my board.  I'm having trouble getting the button and potentiometer to communicate with the 1614.  I tried getting the button to control the led turning on/off and other simple programs, and yet couldn't figure out what wasn't working.  I had breadboarded the circuit beforehand and copied that connection on the board, but it still didn't work.  It's looking grim for the PCB board...

In similar fashion, the green print that hasn't been touched in over a week started acting funny like the red one did.  The new print also did the same wobble.  

![](../images/week13/IMG_1073.jpg)
![](../images/week13/IMG_1074.jpg)
![](../images/week13/IMG_1075.jpg)

This outraged me.  I've worked so hard on this project for so long, only to come up short.  I don't think I'll have enough time to finish this project properly because I've encountered hiccups like this throughtout the whole process, and it doesn't help that my knowledge of electronics is lacking considering I took classes on them.  

Despite all this however, I give myself credit for at least trying and figuring out all the problems.  I can't explain why the button and potentiometer don't work on the board, nor can I explain why these launcher prints are not consistent with properly holding the motor while spinning the Beyblade.  I can confirm however that after multiple tests, I know that my 3D printed apparatuses have been functional this whole time; I never have a problem using this setup when it's outside of the 3D printed handle.  



I guess this is it for now.  
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⡀⠀⠀⠀⢀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⣶⣾⣿⡉⢤⣍⡓⢶⣶⣦⣤⣉⠒⠤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⣿⣿⣿⣿⣿⣷⡀⠙⣿⣷⣌⠻⣿⣿⣿⣶⣌⢳⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣄⠈⢿⣿⡆⠹⣿⣿⣿⣿⣷⣿⡀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⠹⣿⡄⢻⣿⣿⣿⣿⣿⣧⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⠿⣿⣿⣷⣽⣷⢸⣿⡿⣿⡿⠿⠿⣆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠐⠾⢭⣭⡼⠟⠃⣤⡆⠘⢟⢺⣦⡀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢛⣥⣶⠾⠿⠛⠳⠶⢬⡁⠀⠀⠘⣃⠤⠤⠤⢍⠻⡄⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⡿⣫⣾⡿⢋⣥⣶⣿⠿⢿⣿⣿⣿⠩⠭⢽⣷⡾⢿⣿⣦⢱⡹⡀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⡟⠈⠛⠏⠰⢿⣿⣿⣧⣤⣼⣿⣿⣿⡏⠩⠽⣿⣀⣼⣿⣿⢻⣷⢡⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⢁⣿⣷⣦⡀⠀⠉⠙⠛⠛⠛⠋⠁⠙⢻⡆⠀⢌⣉⠉⠉⠀⠸⣿⣇⠆⠀⠀
⠀⠀⠀⠀⢀⣾⣿⣿⣿⣿⣿⡇⢸⣿⣿⣿⣿⠷⣄⢠⣶⣾⣿⣿⣿⣿⣿⠁⠀⠀⢿⣿⣿⣿⣷⠈⣿⠸⡀⠀
⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⠀⣌⡛⠿⣿⣿⠀⠈⢧⢿⣿⡿⠟⠋⠉⣠⣤⣤⣤⣄⠙⢿⣿⠏⣰⣿⡇⢇⠀
⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⡇⢸⣿⣿⣶⣤⣙⠣⢀⠈⠘⠏⠀⠀⢀⣴⢹⡏⢻⡏⣿⣷⣄⠉⢸⣿⣿⣷⠸⡄
⠀⠀⣸⣿⣿⣿⣿⣿⣿⣿⠁⣾⣟⣛⠛⠛⠻⠿⠶⠬⠔⠀⣠⡶⠋⠿⠈⠷⠸⠇⠻⠏⠻⠆⣀⢿⣿⣿⡄⢇
⠀⢰⣿⣿⣿⣿⠿⠿⠛⠋⣰⣿⣿⣿⣿⠿⠿⠿⠒⠒⠂⠀⢨⡤⢶⣶⣶⣶⣶⣶⣶⣶⣶⠆⠃⣀⣿⣿⡇⣸
⢀⣿⣿⠿⠋⣡⣤⣶⣾⣿⣿⣿⡟⠁⠀⣠⣤⣴⣶⣶⣾⣿⣿⣷⡈⢿⣿⣿⣿⣿⠿⠛⣡⣴⣿⣿⣿⣿⠟⠁
⣼⠋⢁⣴⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣎⠻⠟⠋⣠⣴⣿⣿⣿⣿⠿⠋⠁⠀⠀
⢿⣷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣴⠀⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣠⣾⣿⠿⠿⠟⠋⠁⠀⠀⠀⠀⠀
⠀⠉⠛⠛⠿⠿⠿⢿⣿⣿⣿⣵⣾⣿⣧⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠉⠉⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
