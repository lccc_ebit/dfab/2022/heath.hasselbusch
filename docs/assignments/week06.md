# Week 4: Beyblade Launch Mechanism First Tests

***March 3rd-9th***

## Huzzah!! The First Mechanized Beyblade Launch

I can replace my old launchers with this new creation of mine because I launch multiple Beyblades at once. After coming home from my great discovery, I recorded this video demonstrating how my design works. Be sure to watch it:

<iframe width="560" height="315" src="https://www.youtube.com/embed/uSVrp-5X2yM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I was thrilled with my result. I knew now that I didn't need to incorporate any electrical braking. Knowing this, it was definitely going to be easier programming the PCB board. I'd already started getting an idea of what this board might consist of, but still, I needed help with gaining any ground on the PCB components.

This was when I started getting a little aggravated with myself. I kept trying to force myself to understand, and I wasn't getting anywhere. I couldn't think straight for a second, with all the other things I had to worry about and all the new problems that came about the more I worked on this project.

There's not much I can say about this week. I had my focus elsewhere during this week because spring break was coming up, and there was a test for my Physics class I was stressed about (that I ended up bombing anyways because I was so damn nervous). I got myself back into more of a rhythm during spring break, and you'll start to see the gears turning again after I gave myself the time to let go and return at a better time.


> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
