# Week 2: Final Project Proposal, Getting GitHub Websites Ready for Editing, Brainstorming Design Ideas

***February 10th-22nd, 2023***

This week, I prepared my GitHub website to start documenting, submitted a proposal for my project, and created some sketches for my final design.  

## Getting Ready to *Propose*...

I was set on this idea from the start; I had a vision of what this would become, and that's all it took to convince me to go through with this.

Before I had to submit a thorough project proposal, I had to think about everything that would go into making this work. This means I had to identify all the machines I'd use, processes of fabrication, have a bill of materials, and obtain some collective conciseness on the basic fundamentals of this launcher, how it would work, and what potential safety measures need to be in place if things were to go south while experimenting.

![](../images/week04/proposalss.png)
![](../images/week04/proposalss2.png)
![](../images/week04/proposalss3.png)

It took me several attempts before my proposal was finally accepted for credit. You can see in blue the comments my instructor left as advice for filling out my proposal. I blame myself for getting a low grade on this for multiple reasons. 

1. I wasn't fully thinking through all the little details, and he had to comment on multiple submissions
2. I needed to incorporate more machines/machining processes to fulfill the requirements of the class.

![](../images/week04/IMG_0710.jpg)
*Exhibit A*

*Text Translation: NE555, 1k,10k resistors, Diodes, FET, Terminal blocks, 10nf 1nf capacitors, 3V LED (R, G, Y), 8 pin case, 79L12 regulator, 9V battery*

3. I needed to explain more thoroughly how everything would work.  
![](../images/week04/IMG_0711.jpg)
*Exhibit B*

This crucial bit of information kept me from reaching that next step; I had to repeatedly submit updated proposals to my instructor because I wouldn't answer everything I needed to know. This frustrated me because I felt I would address those changes on my website. I was right to have a more accurate depiction on my website rather than a submitted proposal. However, the lesson he was trying to teach was making sure I double-checked for mistakes; chances are you missed something, and that small, seemingly indescribable detail could change your plan in the long run.

## Defining the Problems

For starters, how would I power this device? Would I use DC or AC voltage to power it? What kind of motor would I need/be capable of doing what I want it to do? How will I control the speed of the motor the way I want to?

What was my PCB board going to look like/consist of? How will this device be capable of holding & violently spinning a Beyblade and releasing it at high speeds, ensuring the safety of everyone around? What if a fire breaks out, or there's a dangerous voltage, or even the Beyblade spontaneously combusts?

The only thing I was confident in was designing a 3D model, but I needed to figure out how to answer all these questions I had. 
 
I knew that I would need a motor to initiate the rotating part and a sort of switch to control that speed:

![](../images/week04/IMG_0709.jpg)
![](../images/week04/IMG_0712.jpg)

I wanted this to be a handheld design, taking after the design I saw in the video of the gear-ratio Beyblade launcher. I also pulled inspiration for the design of the handle from a TV remote I use to control a soundbar in my room: 

![](../images/week04/remote1.jpg)
This remote is for the SONY RMT-AH411U AV Soundbar System

As you may have noticed in some of my earlier sketches, there were some wonky designs that I didn't think would ergonomically make it past the drawing board, hence my decision to reference something I already own.

After a few days, I came up with a rough but presentable concept sketch:

![](../images/week04/CrazyName.jpg)

![](../images/week04/IMG_0730.jpg)

You can see on the side view how the motor would sit inside the 3D-printed base. The insert will be large enough to allow the motor to slide into place but small enough that the motor shouldn't be sitting loose enough for it to demonstrate imbalance while performing. There's a small hole meant for the shaft to stick out of, but the material surrounding the small circle is preventing the motor from falling out of place. It seems unnecessary to mention that, but it's always better to explain more than less.

Here is where I ran into my next problem:  How do I get the parts (motor, PCB board, etc...) inside and safely secure them afterward? Does the lab have a printer big enough that's capable of printing this whole thing at once?

To solve the problem of being unable to get inside the remote after it's 3D printed, I added holes on polar opposite sides of each half for screw inserts. This added feature wouldn't only make the inside accessible to add/remove components and keep everything together with all the violent spinning. Still, I could now give myself more room on the printing bed for the 3D printers. However, there was still another problem; what switch was I going to use

Evidently, this meant I had to add/change some features, like for one, my motor control method.
I originally designed to have a limit switch of some sort that when applied pressure from the thumb, it acts as a throttle to control the speed. I quickly shot this idea down. The switch I was looking for was called a momentary rocker switch, and they're close to $100, so that wasn't going to be an option.

I needed something that could control the speed of the motor while the switch was being pressed or pushed. I wasn't aware of all the switches that existed, but I found myself spending too much time on what kind of switch I was going to use. Then I got the brilliant idea of another at-home fix by utilizing what I have at my disposal: Slot cars.

Not precisely the cars themselves but the thumb joysticks used to control the car's speed. Below is a video I made to explain and demonstrate how I'll use a Carrera Go! 1/43 scale slot car remote to control the rpm of the motor in my Beyblade launcher

<iframe width="560" height="315" src="https://www.youtube.com/embed/vMHRqhHvP80" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Here's a clearer image of that same remote:

![](../images/week04/IMG_0594.jpg)

I wondered if I was thinking right about this, but my instructors assured me it would work. That microcontroller with the wires attached to it is where the magic takes place. It's a B10k potentiometer, and I determined it'll perfectly serve the new purpose I have in store for it.

I know my design of the base will change, but what I have is good for now. I have a general idea of how the bottom should look and how big/small everything needs to be, which is a step forward from where I was before.

This is the end of the page

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
