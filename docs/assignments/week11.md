# Week 9: Planning Vinyl Cutting, Arduino Coding

***April 15th-April 22nd***

With the downtime I had, I documented everything from March 24th till today.  In the meantime, I took osme shots of the yellow and red bases to compare them to eachother:

![](../images/week11/IMG_0894.jpg)
![](../images/week11/IMG_0893.jpg)
![](../images/week11/IMG_0895.jpg)
![](../images/week11/IMG_0896.jpg)

From this perspective you can see the inside of the red base, and the hollowed out area to give the motor some more breathing room.

Speaking of breathing, notice the ventilation holes that go around the inner diameter.  This design truly solved the issues I was having before, and all it took was giving the motor some space...something you probably shouldn't say to your significant other.

I intend on printing one last bottom part.  This time with the following chagnes:

- Enlarged/Repositioned screw insert holes (not lined up right in both prints)
- Widening the area where the pcb board will sit because currently the space is barely too narrow

The one thing I'll have to add to the top base is a new design that allows a button switch to rest on top of the top plate.

I also want to make a piece that slides onto the bottom of the base.  A curved barrier that provides more protection to the user so if something disconnets while spinning, your little fingies should be spared.

## Beyblade Launcher Mark III

For my new launcher design, I wanted to make the following changes:

- More space for the PCB board to fit in
- Modified screw insert holes
- Designated area for button to sit on top piece

I did not print out a safety guard.

![](../images/week12/IMG_0917.jpg)
![](../images/week12/IMG_0922.jpg)

I printed out a new model with all the concepts above.  However, I think I used a poorly-calibrated printer because the quality of the launcher was terrible:

![](../images/week12/IMG_0931.jpg)
![](../images/week12/IMG_0935.jpg)

The high, thin walls where the top case secures into place was under extruted; meaning the filimant didn't fill as much as it should've.  Not long after playing around with it, the sides started to break away.  Looking at my design you'll see the little spaces I put underneath the edges of that high part made a really thin geometry; so thin the 3D printer couldn't print any finer to create the feature.

To my surprise, this model actually launched the Beyblade like it was supposed to:

<iframe width="560" height="315" src="https://www.youtube.com/embed/zX1mxFuYGnw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I decided to leave the device alone, since I couldn't get my original launcher to work again for some unknown reason, I didn't want to risk breaking it by showing it off to someone or carrying it around with me everywhere I go.

Now I can move on to the other parts of my project that aren't so mechanical.

> This is the end of the page
<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
     edited by <p style="font-weight: bold; color: #7851a9;">Heath Hasselbusch</p> on <a href="https://gitlab.com/lccc_ebit/dfab/2022/heath.hasselbusch" target="_blank" style="font-weight: bold; color: #7851a9;">Github</a> on <p style="font-size:13px; font-weight: heavy; margin: 5px; color: #7851a9;">3/17/2023</p>
</p>
</div>
